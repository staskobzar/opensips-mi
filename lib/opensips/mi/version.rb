# frozen_string_literal: true

module Opensips
  module MI
    VERSION = "1.1.0"
  end
end
